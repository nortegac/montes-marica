package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.data_structures.LinearProbingHashST;
import model.data_structures.MaxHeapCP;
import model.logic.MVCModelo;
import model.logic.NodoLatLong;
import model.logic.Viaje;
import model.logic.Zona;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		int N = 0;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					view.printMessage("--------- \nCargando datos");
					modelo.cargarDatos();
					view.printMessage("---------");
					break;
					
				case 2:
					view.printMessage("---------");
					view.printMessage("Ingrese el numero N de letras que desea consultar:");
					N = lector.nextInt();
					MaxHeapCP<ArregloDinamico<Zona>> heap = modelo.req1A();
					view.dar1A(N, heap);
					view.printMessage("---------");
					break;

				case 3:
					view.printMessage("---------");
					view.printMessage("Ingrese la latitud para realizar la consulta:");
					Double latitud = lector.nextDouble();					
					view.printMessage("Ingrese la longitud para realizar la consulta:");
					Double longitud = lector.nextDouble();
					LinearProbingHashST<String, NodoLatLong> tabla = modelo.req2A(longitud, latitud);
					view.dar2A(tabla);
					view.printMessage("---------");
					break;
					
				case 4:
					view.printMessage("---------");
					view.printMessage("Ingrese el limite inferior del rango");
					double inf = lector.nextDouble();
					view.printMessage("Ingrese el limite superior del rango");
					double sup = lector.nextDouble();
					Iterator<Viaje> iter = modelo.req3A(inf, sup);
					view.dar3A(iter);
					view.printMessage("---------");
					break;

				case 5: 
					view.printMessage("---------");
					view.printMessage("Ingrese el numero N de zonas que desea consultar");
					N = lector.nextInt();
					MaxHeapCP<Zona> heap2 = modelo.req1B();
					view.dar1B(heap2, N);
					view.printMessage("---------");			
					break;	
					
				case 6: 
					view.printMessage("---------");
					view.printMessage("Ingrese la latitud para realizar la consulta:");
					latitud = lector.nextDouble();					
					view.printMessage("Ingrese la longitud para realizar la consulta:");
					longitud = lector.nextDouble();
					LinearProbingHashST<Integer, NodoLatLong> tabla2 = modelo.req2B(longitud, latitud);
					view.dar2B(tabla2);
					view.printMessage("---------");			
					break;	
					
				case 7:
					view.printMessage("---------");
					view.printMessage("Ingrese el limite inferior del rango");
					double infd = lector.nextDouble();
					view.printMessage("Ingrese el limite superior del rango");
					double supd = lector.nextDouble();
					Iterator<Viaje> iterd = modelo.req3B(infd, supd);
					view.dar3B(iterd);
					view.printMessage("---------");		
					break;

				case 8:
					view.printMessage("---------");
					view.printMessage("Ingrese la zona de origen");
					int sourceId = lector.nextInt();
					view.printMessage("Ingrese la hora");
					int hora = lector.nextInt();
					LinearProbingHashST<String, Viaje> tabla3 = modelo.req1C(sourceId, hora);
					view.dar1C(tabla3);
					view.printMessage("---------");
					break;

				case 9:
					view.printMessage("---------");
					view.printMessage("Ingrese la zona de destino");
					sourceId = lector.nextInt();
					view.printMessage("Ingrese la hora menor del rango");
					int horaMenor = lector.nextInt();
					view.printMessage("Ingrese la hora mayor del rango");
					int horaMayor = lector.nextInt();
					tabla3 = modelo.req2C(sourceId, horaMenor, horaMayor);
					view.dar1C(tabla3);
					view.printMessage("---------");
					break;

				case 10: 
					view.printMessage("---------");
					view.printMessage("Ingrese el numero N de zonas que desea consultar");
					N = lector.nextInt();
					MaxHeapCP<ArregloDinamico<String>> heap3 = modelo.req3C();
					view.dar3C(heap3, N);
					view.printMessage("---------");			
					break;	
					
				case 11: 
					view.printMessage("---------");
					modelo.req4C();
					view.printMessage("---------");			
					break;	
					
				case 12: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
