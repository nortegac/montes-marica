package model.data_structures;

import java.util.Iterator;

public interface IHashTables<K,V> {
	
	/**
	 * Agregar una dupla (K, V) a la tabla. En este caso, una llave K puede tener asociado un conjunto de valores Vs. 
	 * El valor V siempre se agrega al conjunto de Vs de la llave K. El conjunto de Vs NO puede estar vacio.
	 * @param key Llave del valor a agregar
	 * @param value Valor a agregar
	 */
	public void put(K key, V value);
	
	/**
	 * Retornar el conjunto de valores Vs asociados a la llave K. Se obtiene un Iterator vacio solo si la llave K no existe. 
	 * Funciona solo en el caso que se agreguen duplas con putInSet( ... ).
	 * @param key Llave del conjunto de valores buscado.
	 * @return Conjunto de valores Vs asociados a la llave K
	 */
	public V getVal(K key);
	
	/**
	 * Borrar la dupla asociada a la llave K. Se obtiene el conjunto de valores V asociado a la llave K. Se obtiene un Iterator vacio solo si la llave K no existe. 
	 * Funciona solo en el caso que se agreguen duplas con putInSet( ... ).
	 * @param key Llave del conjunto de valores que se esta borrando.
	 * @return Conjunto de valores Vs asociados a la llave K
	 */
	public V deleteVal(K key);
	
	/**
	 * @return Conjunto de llaves K presentes en la tabla
	 */
	public Iterator<K> keys();
}
