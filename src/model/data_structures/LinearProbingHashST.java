package model.data_structures;

import java.util.Iterator;
import java.util.*;

public class LinearProbingHashST<K extends Comparable<K>,V>  implements IHashTables<K,V>{

	private int M;
	private int N;
	private V[] vals;
	private K[] keys;
	public int rehashes;

	public LinearProbingHashST(int m)
	{
		rehashes = 0;
		M = m;
		vals = (V[]) new Object[M];
		keys = (K[]) new Comparable[M];
	}

	@Override
	public void put(K key, V value)
	{
		if (N >= ((3*M)/4))
			resize(nextPrime(2*M));

		int i;
		for (i = hash(key); keys[i] != null; i = (i+1)%M) 
		{
			if (keys[i].equals(key))
			{
				vals[i] = value;
				return;
			}
		}
		keys[i] = key;
		vals[i] = value;
		N++;
	}

	@Override
	public V getVal(K key) {
		for (int i = hash(key); keys[i] != null; i = (i+1)%M)
		{
			if (keys[i].equals(key))
				return vals[i];
		}
		return null;
	}

	@Override
	public V deleteVal(K key) {
		if (!contains(key))
			return null;

		int i = hash(key);
		while (!key.equals(keys[i]))
			i = (i+1)%M;

		V res = vals[i];
		keys[i] = null;
		vals[i] = null;
		i = (i+1)%M;

		while(keys[i] != null)
		{
			K ktr = keys[i];
			V vtr = vals[i];
			keys[i] = null;
			vals[i] = null;
			N--;
			put(ktr, vtr);

			i= (i+1)%M;
		}
		N--;

		if (N>0 && N==M/8)
			resize(M/2);

		return res;
	}

	@Override
	public Iterator<K> keys() {
		ArrayList<K> lista = new ArrayList<K>();
		for (int i=0; i<keys.length; i++)
		{
			if (keys[i] != null)
				lista.add(keys[i]);
		}
		return lista.iterator();
	}

	public boolean contains(K key)
	{
		Iterator<K> iter = keys();
		while (iter.hasNext())
		{
			K actual = iter.next();
			if ( actual.equals(key) )
				return true;
		}
		return false;
	}

	public int getM()
	{
		return M;
	}

	public int getN()
	{
		return N;
	}

	public Comparable<K>[] getKeys()
	{
		return keys;
	}

	public void resize(int capacidad)
	{
		LinearProbingHashST<K, V> t;
		t = new LinearProbingHashST<K, V>(capacidad);
		for (int i = 0; i < M; i++)
		{
			if (keys[i] != null)
				t.put(keys[i], vals[i]);				
		}
		keys = t.keys;
		vals = t.vals;
		M    = t.M;	
		rehashes++;
	}

	private int hash(K key)
	{
		return (key.hashCode() & 0x7fffffff) % M;
	}

	private boolean prime(int n)
	{
		for (int i=2; i<n; i++)
			if (n % i == 0)
				return false;
		return true;
	}

	private int nextPrime(int n)
	{
		while (!prime(n))
			n++;
		return n;
	}


}
