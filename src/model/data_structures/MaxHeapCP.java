package model.data_structures;

public class MaxHeapCP <T extends Comparable<T>>{

	private int numeroElementos;

	private ArregloDinamico<T> datos;

	public MaxHeapCP()
	{
		numeroElementos = 0;
		datos = new ArregloDinamico<T>(2);
	}


	public int darNumElementos()
	{
		return numeroElementos; 
	}

	public boolean esVacia()
	{
		return numeroElementos == 0;
	}
	public void agregar(T elemento)
	{
		if(esVacia())
		{
			datos.agregar(null);
			datos.agregar(elemento);
			numeroElementos++;
		}
		else
		{
			datos.agregar(elemento);
			++numeroElementos;
			swim(numeroElementos);			
		}
	}

	public T sacarMax ()
	{
		T max = datos.get(1);
		cambiar(1, numeroElementos--);
		datos.set(numeroElementos+1,null);
		sink (1);
		return max;
	}

	public T darMax()
	{
		return datos.get(1);
	}

	private void swim (int i)
	{
		while(i > 1 && (datos.get(i/2).compareTo(datos.get(i)))<0)
		{
			cambiar(i/2 , i);
			i = i/2;
		}
	}

	private void sink(int i)
	{
		while(2*i <= numeroElementos)
		{
			int j = 2*i;
			if(j<numeroElementos && (datos.get(j).compareTo(datos.get(j+1)))<0)
			{
				j++;
			}
			if(!((datos.get(i).compareTo(datos.get(j)))<0))
			{
				break;
			}
			cambiar(i, j);
			i = j;
		}
	}
	private void cambiar(int i, int j)
	{
		T aux = datos.get(i);
		datos.set(i, datos.get(j));
		datos.set(j,aux);
	}

}