package model.data_structures;

public class Node<T> {
	private Node<T> next;
	private T data;

	public Node(T iData, Node<T> iNext) {
		data = iData;
		next = iNext;
	}

	public Node(T iData) {
		data = iData;
		next = null;
	}

	public T getData() {
		return data;
	}

	public Node<T> getNext() {
		return next;
	}

	public void setNext(Node<T> iNext) {
		next = iNext;
	}

	public boolean hasNext() {
		if (next != null)
			return true;
		return false;
	}

}
