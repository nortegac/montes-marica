package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class RedBlackBST<K extends Comparable<K>, V> {

	private final static boolean RED = true;
	private final static boolean BLACK = false;
	private Node root;

	private class Node{
		K key;
		V value;
		Node left;
		Node right;
		int N;
		boolean color;

		Node(K pKey, V val, int N, boolean pColor)
		{
			this.key = pKey;
			this.value = val;
			this.N = N;
			this.color = pColor;
		}
	}

	public RedBlackBST()
	{
		root = null;
	}
	
	private boolean isRed(Node node)
	{
		if (node == null)
			return false;
		return node.color == RED;
	}

	private Node rotateLeft(Node node)
	{
		Node r = node.right;
		node.right = r.left;
		r.left = node;
		r.color = node.color;
		node.color = RED;
		r.N = node.N;
		node.N = 1 + size(node.left) + size(node.right);
		return r;
	}

	private Node rotateRight(Node node)
	{
		Node r = node.left;
		node.left = r.right;
		r.right = node;
		r.color = node.color;
		node.color = RED;
		r.N = node.N;
		node.N = 1 + size(node.left) + size(node.right);
		return r;
	}

	private void flipColors(Node h)
	{
		h.color = RED;
		h.left.color = BLACK;
		h.right.color = BLACK;
	}

	public boolean isEmpty()
	{
		return root == null;
	}

	public int getHeight(K key)
	{
		return getHeight(key, root, 0);
	}

	private int getHeight(K key, Node x, int height)
	{
		if (x == null)
			return -1;
		else {
			int cmp = key.compareTo(x.key);
			if (cmp == 0)
				return height;
			else if (cmp < 0)
				return getHeight(key, x.left, ++height);
			else
				return getHeight(key, x.right, ++height);				
		}

	}


	public boolean contains(K key)
	{
		return contains(key, root);
	}

	private boolean contains(K key, Node x)
	{
		if (x == null)
			return false;
		else
		{
			int cmp = key.compareTo(x.key);
			if (cmp > 0)
				return contains(key, x.right);
			else if (cmp < 0)
				return contains(key, x.left);
			else
				return true;
		}

	}

	public int height()
	{
		return height(root, 0);
	}

	private int height(Node x, int altura)
	{
		if (x == null)
			return (altura > 0) ? altura-1 : 0;
		else
		{
			int left = height(x.left,altura+1);
			int right = height(x.right,altura+1);

			return (left > right) ?  left : right;
		}
	}

	
	public boolean check()
	{
		return estaOrdendo(root, null, null) && es23(root) && estaBalanceado();
	}

	private boolean estaOrdendo(Node x, K min, K max)
	{
		if (x == null) 
			return true;
        if (min != null && x.key.compareTo(min) <= 0)
        	return false;
        if (max != null && x.key.compareTo(max) >= 0)
        	return false;
        return estaOrdendo(x.left, min, x.key) && estaOrdendo(x.right, x.key, max);
	}
	

    private boolean es23(Node x) 
    {
        if (x == null)
        	return true;
        if (isRed(x.right)) 
        	return false;
        if (x != root && isRed(x) && isRed(x.left))
            return false;
        return es23(x.left) && es23(x.right);
    } 

    private boolean estaBalanceado() { 
        int black = 0;     
        Node x = root;
        while (x != null) {
            if (!isRed(x)) black++;
            x = x.left;
        }
        return estaBalanceado(root, black);
    }

    private boolean estaBalanceado(Node x, int black) {
        if (x == null) 
        	return black == 0;
        if (!isRed(x)) 
        	black--;
        return estaBalanceado(x.left, black) && estaBalanceado(x.right, black);
    } 
	
	public int size()
	{
		return size(root);
	}

	private int size(Node x)
	{
		if (x == null) 
			return 0;
		else 
			return x.N; 
	}

	public void put(K key, V value)
	{
		root = put(root, key, value);
		root.color = BLACK;
	}


	public Node put(Node h, K key, V value)
	{
		if (h == null)
			return new Node(key, value, 1, RED);

		int comparacion = key.compareTo(h.key);
		if (comparacion < 0)
			h.left = put(h.left, key, value);
		else if (comparacion > 0)
			h.right = put(h.right, key, value);
		else
			h.value = value;

		if (isRed(h.right) && !isRed(h.left)) 
			h = rotateLeft(h); 
		if (isRed(h.left) && isRed(h.left.left)) 
			h = rotateRight(h); 
		if (isRed(h.left) && isRed(h.right)) 
			flipColors(h);

		h.N = size(h.left) + size(h.right) + 1;
		return h;
	}

	public V get(K key)
	{
		return get(root, key);
	}

	private V get(Node x, K key)
	{
		if (x == null)
			return null;
		int comparacion = key.compareTo(x.key);
		if (comparacion < 0)
			return get(x.left, key);
		else if (comparacion > 0)
			return get(x.right, key);
		else
			return x.value;
	}
	public Node getNode(Node x, K key)
	{
		if (x == null)
			return null;
		int comparacion = key.compareTo(x.key);
		if (comparacion < 0)
			return getNode(x.left, key);
		else if (comparacion > 0)
			return getNode(x.right, key);
		else
			return x;
	}
	public Node getNode(K key)
	{
		return getNode(root, key);
	}
	public K min()
	{
		return (root == null) ? null : min(root).key;
	}

	private Node min(Node x)
	{
		if (x.left == null)
			return x;
		return min(x.left);
	}

	public K max()
	{
		return (root == null) ? null: max(root).key;
	}

	private Node max(Node x)
	{
		if (x.right == null)
			return x;
		return max(x.right);
	}

	public K floor(K key)
	{
		Node x = floor(root, key);
		if (x == null)
			return null;
		return x.key;
	}

	private Node floor(Node x, K key)
	{
		if (x == null)
			return null;
		int comparacion = key.compareTo(x.key);
		if (comparacion == 0)
			return x;
		if (comparacion < 0)
			return floor(x.left, key);
		Node t = floor(x.right, key);
		if (t != null)
			return t;
		else
			return x;
	}

	public K ceiling(K key)
	{
		Node x = ceiling(root, key);
		if (x == null)
			return null;
		return x.key;
	}

	private Node ceiling(Node x, K key)
	{
		if (x == null)
			return null;
		int comparacion = key.compareTo(x.key);
		if (comparacion == 0)
			return x;
		if (comparacion > 0)
			return ceiling(x.right, key);
		Node t = ceiling(x.left, key);
		if (t != null)
			return t;
		else
			return x;
	}


	public K select(int k)
	{
		return select(root, k).key;
	}

	private Node select(Node x, int k)
	{
		if (x == null)
			return null;
		int t = size(x.left);
		if (t > k)
			return select(x.left, k);
		else if (t < k)
			return select(x.right, k-t-1);
		else
			return x;
	}

	public int rank(K key)
	{
		return rank(key, root);
	}

	private int rank(K key, Node x)
	{
		if (x == null)
			return 0;
		int comparacion = key.compareTo(x.key);
		if (comparacion < 0)
			return rank(key, x.left);
		else if (comparacion > 0)
			return 1 + size(x.left) + rank(key, x.right);
		else
			return size(x.left);
	}

	public void deleteMin()
	{
		root = deleteMin(root);
	}

	private Node deleteMin(Node x)
	{
		if (x.left == null)
			return x.right; 
		x.left = deleteMin(x.left);
		x.N = size(x.left) + size(x.right) + 1;
		return x;
	}

	public void delete(K key)
	{  
		root = delete(root, key);  
	}

	private Node delete(Node x, K key)
	{
		if (x == null)
			return null;
		int cmp = key.compareTo(x.key);
		if (cmp < 0) 
			x.left = delete(x.left, key);
		else if (cmp > 0) 
			x.right = delete(x.right, key);
		else
		{
			if (x.right == null) 
				return x.left;
			if (x.left == null)
				return x.right; Node t = x;
				x = min(t.right); 
				x.right = deleteMin(t.right);
				x.left = t.left;
		}
		x.N = size(x.left) + size(x.right) + 1; return x;
	}

	public Iterator<K> keys()
	{
		return keysInRange(min(), max());
	}

	public Iterator<K> keysInRange(K lo, K hi)
	{
		ArrayList<K> lista = new ArrayList<K>();
		keys(root, lista, lo, hi);
		return lista.iterator();
	}

	private void keys(Node x, ArrayList<K> lista, K lo, K hi)
	{
		if (x == null)
			return;
		int comparacionLo = lo.compareTo(x.key);
		int comparacionHi = hi.compareTo(x.key);

		if (comparacionLo < 0)
			keys(x.left, lista, lo, hi);
		if (comparacionLo <= 0 && comparacionHi >= 0)
			lista.add(x.key);
		if (comparacionHi > 0)
			keys(x.right, lista, lo ,hi);
	}
	
	public Iterator<V> valuesInRange(K lo, K hi)
	{
		ArrayList<V> lista = new ArrayList<V>();
		values(root, lista, lo, hi);
		return lista.iterator();
	}
	
	private void values(Node x, ArrayList<V> lista, K lo, K hi)
	{
		if (x == null)
			return;
		int comparacionLo = lo.compareTo(x.key);
		int comparacionHi = hi.compareTo(x.key);
		
		if (comparacionLo < 0)
			values(x.left, lista, lo, hi);
		if (comparacionLo <= 0 && comparacionHi >= 0)
			lista.add(x.value);
		if (comparacionHi > 0)
			values(x.right, lista, lo ,hi);
	}
	

	
}

