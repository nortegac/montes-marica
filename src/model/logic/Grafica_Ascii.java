package model.logic;

public class Grafica_Ascii 
{
	public void imprimir(int[] t)
	{
		System.out.println("Zona de Origen | % de datos faltantes");
		for(int i = 0; i < t.length; i++)
		{
			if ((i+1)<10)
				System.out.print("0" + (i+1) + " |");
			else
				System.out.print("" + (i+1) + " |");
			if(t[i] == 0)
			{
				System.out.print("hora sin viajes");
			}
			for(int j = 0; j < t[i]; j++)
			{
				System.out.print("*");
			}
			System.out.println("");
		}
	}
}
