package model.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.LinearProbingHashST;
import model.data_structures.MaxHeapCP;
import model.data_structures.Node;
import model.data_structures.RedBlackBST;


public class MVCModelo {

	private ArregloDinamico<NodoLatLong> nodosLatLong;
	private ArregloDinamico<Viaje> viajesHoras1;
	private ArregloDinamico<Viaje> viajesHoras2;
	private ArregloDinamico<Viaje> viajesMes1;
	private ArregloDinamico<Zona> zonas;


	public MVCModelo()
	{
		nodosLatLong = new ArregloDinamico<NodoLatLong>(1000);
		viajesHoras1 = new ArregloDinamico<Viaje>(1000);
		viajesHoras2 = new ArregloDinamico<Viaje>(1000);
		viajesMes1 = new ArregloDinamico<Viaje>(1000);
		zonas = new ArregloDinamico<Zona>(1000);
	}

	public void cargarDatos()
	{
		CSVReader lector = null;
		Viaje viaje = null;

		try {

			//Lectura archivo Nodos de malla vial
			lector = new CSVReader(new FileReader("./data/Nodes_of_red_vial-wgs84_shp.txt"));
			System.out.println("Cargando nodos (esquinas) de la malla vial...");
			String[] linea = { };		
			while ((linea = lector.readNext()) != null)
			{
				NodoLatLong nodo = new NodoLatLong(Integer.parseInt(linea[0]), Double.parseDouble(linea[1]), Double.parseDouble(linea[2]));
				nodosLatLong.agregar(nodo);
			}
			System.out.println("Datos cargados. Numero de nodos cargados del archivo .txt : " + nodosLatLong.size());


			//Lectura de archivos de viajes por hora
			for (int i=1; i<=2; i++)
			{
				lector = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+i+"-All-HourlyAggregate.csv"));
				lector.readNext();
				System.out.println("Cargando viajes por hora del trimestre " + i + "...");
				while ((linea = lector.readNext()) != null)
				{
					viaje = new Viaje(i, Integer.parseInt(linea[0]), Integer.parseInt(linea[1]), Integer.parseInt(linea[2]), Double.parseDouble(linea[3]), Double.parseDouble(linea[4]));
					if (i == 1)
						viajesHoras1.agregar(viaje);
					else
						viajesHoras2.agregar(viaje);
				}
				if (i == 1)
					System.out.println("Datos cargados. Numero de viajes cargados del archivo : " + viajesHoras1.size());
				else
					System.out.println("Datos cargados. Numero de viajes cargados del archivo : " + viajesHoras2.size());
			}


			//Lectura archivo viajes por mes
			lector = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv"));
			lector.readNext();
			System.out.println("Cargando viajes por mes del trimestre 1");
			while ((linea = lector.readNext()) != null)
			{
				viaje = new Viaje(1, Integer.parseInt(linea[0]), Integer.parseInt(linea[1]), Integer.parseInt(linea[2]), Double.parseDouble(linea[3]), Double.parseDouble(linea[4]));
				viajesMes1.agregar(viaje);
			}
			System.out.println("Datos cargados. Numero de viajes cargados del archivo : " + viajesMes1.size());


			//Lectura de zonas del archivo .json
			System.out.println("Cargando zonas definidas por UBER del archivo .json ...");
			leerArchivoJson("./data/bogota_cadastral.json");
			System.out.println("Datos cargados. Numero de zonas cargadas del archivo : " + zonas.size());


			lector.close();
		}
		catch (IOException e) {
			System.out.println("No se encontro el archivo para cargar los datos");
		}
	}

	private void leerArchivoJson(String ruta)
	{
		try {
			ArrayList<ArrayList<NodoLatLong>> poligonos = new ArrayList<ArrayList<NodoLatLong>>();


			FileReader fr = new FileReader(ruta);
			JsonReader reader = new JsonReader(fr);

			reader.beginObject();
			reader.nextName();						//type
			reader.nextString();					//Feature collection

			reader.nextName();						//features
			reader.beginArray();					//[
			while (reader.hasNext())
			{
				reader.beginObject();				//{
				while (reader.hasNext())
				{
					String name = reader.nextName();
					switch (name)
					{
					case "geometry":
						poligonos = leerGeometry(reader);
						break;

					case "properties":
						Zona zona = leerProperties(reader, poligonos);
						zonas.agregar(zona);
						break;

					default:
						reader.skipValue();
						break;
					}
				}
				reader.endObject();
			}
			reader.endArray();
			reader.endObject();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}

	}

	private ArrayList<ArrayList<NodoLatLong>> leerGeometry(JsonReader reader) throws IOException
	{
		ArrayList<ArrayList<NodoLatLong>> poligonos = new ArrayList<ArrayList<NodoLatLong>>();

		reader.beginObject();

		reader.nextName();														//type
		reader.nextString();													//Multipolygon
		reader.nextName();														//cooridnates

		reader.beginArray();													//Empieza arreglo de poligonos
		while (reader.hasNext())
		{
			reader.beginArray();												//Empieza arreglo de "lineas"
			while (reader.hasNext())
			{
				ArrayList<NodoLatLong> puntos = new ArrayList<NodoLatLong>();
				reader.beginArray(); 											//Empieza arreglo de puntos
				while (reader.hasNext())
				{
					reader.beginArray(); 										//Empieza arreglo coordenadas
					double lon = reader.nextDouble();
					double lat = reader.nextDouble();
					NodoLatLong coordenada = new NodoLatLong(-1, lon, lat);
					puntos.add(coordenada);
					reader.endArray();
				}
				reader.endArray();
				poligonos.add(puntos);
			}
			reader.endArray();
		}
		reader.endArray();

		reader.endObject();

		return poligonos;
	}

	private Zona leerProperties(JsonReader reader, ArrayList<ArrayList<NodoLatLong>> poligonos) throws IOException
	{
		String nombreZona = "", movId = "";
		double shapeLeng = 0, shapeArea = 0;

		reader.beginObject();
		while (reader.hasNext())
		{
			String name = reader.nextName();
			switch (name)
			{
			case "scanombre":
				nombreZona = reader.nextString();
				break;
			case "shape_leng":
				shapeLeng = reader.nextDouble();
				break;
			case "shape_area":
				shapeArea = reader.nextDouble();
				break;
			case "MOVEMENT_ID":
				movId = reader.nextString();
				break;
			default:
				reader.skipValue();
				break;		
			}
		}
		reader.endObject();

		Zona resp = new Zona(poligonos, Integer.parseInt(movId), nombreZona, shapeLeng, shapeArea);
		return resp;
	}

	public MaxHeapCP<ArregloDinamico<Zona>> req1A()
	{
		MaxHeapCP<ArregloDinamico<Zona>> resp = new MaxHeapCP<ArregloDinamico<Zona>>();
		char letra = 'A';
		for (int i=0; i<26;i++)
		{
			ArregloDinamico<Zona> arreglo = new  ArregloDinamico<Zona>(10);
			for (int j=0; j<zonas.size(); j++)
			{
				Zona actual = zonas.get(j);
				if (letra == actual.scaNombre.charAt(0))
					arreglo.agregar(actual);
			}
			resp.agregar(arreglo);
			letra++;
		}
		return resp;	
	}

	public LinearProbingHashST<String, NodoLatLong> req2A(Double pLongitud, Double pLatitud)
	{
		LinearProbingHashST<String, NodoLatLong> tabla = new LinearProbingHashST<String, NodoLatLong>(101);
		int lonT = (int)(pLongitud*1000);
		int latT = (int)(pLatitud*1000);

		for (int i=0; i<zonas.size(); i++)
		{
			Zona zona = zonas.get(i);
			for (int j=0; j<zona.poligonos.size(); j++)
			{
				ArrayList<NodoLatLong> nodos = zona.poligonos.get(j);
				for (int k=0; k<nodos.size(); k++)
				{
					NodoLatLong coordenada = nodos.get(k);
					Double l1 = coordenada.getLatitud();
					Double l2 = coordenada.getLongitud();

					int  latitud = (int)(coordenada.getLatitud()*1000);
					int longitud = (int)(coordenada.getLongitud()*1000);

					if (lonT == longitud && latT == latitud)
						tabla.put(zona.scaNombre+","+l2+","+l1, coordenada);
				}
			}
		}

		return tabla;
	}

	public Iterator<Viaje> req3A(double lo, double hi)
	{
		RedBlackBST<Double, Viaje> arbolTri1 = new RedBlackBST<Double, Viaje>();
		for(int i = 0; i < viajesMes1.size() ; i++)
		{
			arbolTri1.put(viajesMes1.get(i).getMeanTravelTime(), viajesMes1.get(i));
		}
		return arbolTri1.valuesInRange(lo, hi);
	}

	public MaxHeapCP<Zona> req1B()
	{
		MaxHeapCP<Zona> res = new MaxHeapCP<Zona>();
		for (int i=0; i<zonas.size(); i++)
		{
			Zona zona = zonas.get(i);
			res.agregar(zona);
		}
		return res;
	}

	public LinearProbingHashST<Integer, NodoLatLong> req2B(Double pLongitud, Double pLatitud)
	{
		LinearProbingHashST<Integer, NodoLatLong> tabla = new LinearProbingHashST<Integer, NodoLatLong>(23);
		int lonT = (int)(pLongitud*100);
		int latT = (int)(pLatitud*100);

		for (int i=0; i<nodosLatLong.size(); i++)
		{
			NodoLatLong coordenada = nodosLatLong.get(i);
			int  latitud = (int)(coordenada.getLatitud()*100);
			int longitud = (int)(coordenada.getLongitud()*100);

			if (lonT == longitud && latT == latitud)
				tabla.put(coordenada.getId(), coordenada);
		}

		return tabla;
	}


	public Iterator<Viaje> req3B(double lo, double hi)
	{
		RedBlackBST<Double, Viaje> arbolTri1 = new RedBlackBST<Double, Viaje>();
		for(int i = 0; i < viajesMes1.size() ; i++)
		{
			arbolTri1.put(viajesMes1.get(i).getStandardDeviationTravelTime(), viajesMes1.get(i));
		}
		return arbolTri1.valuesInRange(lo, hi);
	}

	public LinearProbingHashST<String, Viaje> req1C(int sourceId, int hora)
	{
		LinearProbingHashST<String, Viaje> tabla = new LinearProbingHashST<String, Viaje>(20);
		for (int i=0; i<viajesHoras1.size(); i++)
		{
			Viaje viaje = viajesHoras1.get(i);
			if (viaje.getSourceid() == sourceId && viaje.getVariable() == hora)
				tabla.put(viaje.toString(), viaje);
		}
		for (int i=0; i<viajesHoras2.size(); i++)
		{
			Viaje viaje = viajesHoras2.get(i);
			if (viaje.getSourceid() == sourceId && viaje.getVariable() == hora)
				tabla.put(viaje.toString(), viaje);
		}

		return tabla;
	}

	public LinearProbingHashST<String, Viaje> req2C(int dstId, int horaMenor, int horaMayor)
	{
		LinearProbingHashST<String, Viaje> tabla = new LinearProbingHashST<String, Viaje>(20);
		for (int i=0; i<viajesHoras1.size(); i++)
		{
			Viaje viaje = viajesHoras1.get(i);
			if (viaje.getDstid() == dstId && viaje.getVariable() >= horaMenor && viaje.getVariable() <= horaMayor)
				tabla.put(viaje.toString(), viaje);
		}
		for (int i=0; i<viajesHoras2.size(); i++)
		{
			Viaje viaje = viajesHoras2.get(i);
			if (viaje.getDstid() == dstId && viaje.getVariable() >= horaMenor && viaje.getVariable() <= horaMayor)
				tabla.put(viaje.toString(), viaje);
		}

		return tabla;
	}

	public MaxHeapCP<ArregloDinamico<String>> req3C()
	{
		MaxHeapCP<ArregloDinamico<String>> heap = new MaxHeapCP<ArregloDinamico<String>>();
		for (int i=0; i<zonas.size(); i++)
		{
			ArregloDinamico<String> arreglo = new ArregloDinamico<String>(100);
			Zona zona = zonas.get(i);
			arreglo.agregar(zona.scaNombre);
			for (int j=0; j<zona.poligonos.size(); j++)
			{
				ArrayList<NodoLatLong> nodos = zona.poligonos.get(j);
				for (int k=0; k<nodos.size(); k++)
					arreglo.agregar(nodos.get(k).toString());
			}
			heap.agregar(arreglo);
		}
		return heap;
	}

	public void req4C()
	{
		int[] ret = new int[1160];
		
		LinearProbingHashST<Integer, Integer> tabla = new LinearProbingHashST<Integer, Integer>(1160);
		for (int i=0; i<1160; i++)
		{
			tabla.put(i, 0);
		}
		
		for (int i=0; i<viajesHoras1.size(); i++)
		{
			int numZona = viajesHoras1.get(i).getSourceid()-1;
			int cont = tabla.getVal(numZona);
			tabla.put(numZona, ++cont);
		}
		for (int i=0; i<viajesHoras2.size(); i++)
		{
			int numZona = viajesHoras2.get(i).getSourceid()-1;
			int cont = tabla.getVal(numZona);
			tabla.put(numZona, ++cont);
		}
		for (int i=0; i<tabla.getN(); i++)
		{
			double numVeces = tabla.getVal(i);
			double p = ((double)numVeces/55680)*100;
			ret[i] = (int) ((100 - p)/2);
		}
		Grafica_Ascii g = new Grafica_Ascii();
		g.imprimir(ret);
	}
}