package model.logic;

public class NodoLatLong implements Comparable<NodoLatLong>{

	private int id;
	private double longitud;
	private double latitud;

	public NodoLatLong(int pId, double pLongitud, double pLatitud)
	{
		id = pId;
		longitud = pLongitud;
		latitud = pLatitud;
	}

	public int getId()
	{
		return id;
	}

	public double getLongitud()
	{
		return longitud;
	}

	public double getLatitud()
	{
		return latitud;
	}

	public String toString()
	{
		return "ID: " + id + " | Longitud: " + longitud + " | Latitud: " + latitud;
	}
	
	@Override
	public int compareTo(NodoLatLong o) {
		if (id > o.id)
			return 1;
		else if (id < o.id)
			return -1;
		else
			return 0;
	}

}
