package model.logic;

public class Viaje implements Comparable<Viaje>{
	private int sourceid;
	private int dstid;
	private int variable;
	private int trimestre;
	private double mean_travel_time;
	private double standard_deviation_travel_time;


	public Viaje(int iTrimestre, int iSourceid, int iDstid, int iVar, double iTT, double sdTT) {
		trimestre = iTrimestre;
		sourceid = iSourceid;
		dstid = iDstid;
		variable = iVar;
		mean_travel_time = iTT;
		standard_deviation_travel_time = sdTT;
	}



	public int getSourceid() {
		return sourceid;
	}

	public int getDstid() {
		return dstid;
	}

	public int getVariable() {
		return variable;
	}

	public double getMeanTravelTime() {
		return mean_travel_time;
	}

	public double getStandardDeviationTravelTime() {
		return standard_deviation_travel_time;
	}

	public int getTrimestre() {
		return trimestre;
	}


	@Override
	public String toString() {
		return " Zona origen:" + sourceid + " | Zona Destino:" + dstid + " | Mes: " + variable + " | Tiempo Promedio:" + mean_travel_time;
	}

	@Override
	public int compareTo(Viaje o) {
		if (sourceid > o.sourceid)
			return 1;
		else if (sourceid < o.sourceid)
			return -1;
		else
		{
			if (dstid > o.dstid)
				return 1;
			else if (dstid < o.dstid)
				return -1;
			else
				return 0;
		}
	}


}