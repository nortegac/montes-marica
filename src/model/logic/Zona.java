package model.logic;

import java.util.ArrayList;

public class Zona implements Comparable<Zona> {

	public ArrayList<ArrayList<NodoLatLong>> poligonos;
	public int movementID;
	public String scaNombre;
	public double shapeLeng;
	public double shapeArea;

	public Zona (ArrayList<ArrayList<NodoLatLong>> coordenadas, int movId, String nombre, double leng, double area)
	{
		poligonos = coordenadas;
		movementID = movId;
		scaNombre = nombre;
		shapeLeng = leng;
		shapeArea = area;
	}


	@Override
	public int compareTo(Zona o) {
		if (maxLatitud().getLatitud() > o.maxLatitud().getLatitud())
			return 1;
		else if ((maxLatitud().getLatitud() < o.maxLatitud().getLatitud()))
			return -1;
		else
			return 0;
	}


	public NodoLatLong maxLatitud()
	{
		NodoLatLong mayor = poligonos.get(0).get(0);
		for (int i=0; i<poligonos.size(); i++)
		{
			ArrayList<NodoLatLong> coordenadas = poligonos.get(i);
			for (int j=0; j<coordenadas.size(); j++)
			{
				NodoLatLong nodo = coordenadas.get(j);
				Double latActual = nodo.getLatitud();
				if (latActual > mayor.getLatitud())
					mayor = nodo;
			}
		}
		return mayor;
	}
}
