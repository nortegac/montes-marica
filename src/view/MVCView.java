package view;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.*;
import model.logic.MVCModelo;
import model.logic.Viaje;
import model.logic.*;

public class MVCView 
{
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{

	}

	public void printMenu()
	{
		System.out.println("1. Cargar datos");
		System.out.println("2. Obtener las N letras más frecuentes por las que comienza el nombre de una zona.");
		System.out.println("3. Buscar los nodos que delimitan las zonas por Localización Geográfica");
		System.out.println("4. Buscar los tiempos promedio de viaje que están en un rango y que son del primer trimestre del 2018");
		System.out.println("5. Buscar los N zonas que están más al norte.");
		System.out.println("6. Buscar nodos de la malla vial por Localización Geográfica");
		System.out.println("7. Buscar los tiempos de espera que tienen una desviación estándar en un rango dado y que son del primer trimestre del 2018.");
		System.out.println("8. Retornar todos los tiempos de viaje promedio que salen de una zona dada y a una hora dada");			
		System.out.println("9. Retornar todos los tiempos de viaje que llegan de una zona dada y en un rango de horas");			
		System.out.println("10. Obtener las N zonas priorizadas por la mayor cantidad de nodos que definen su frontera");			
		System.out.println("11. Grafica Ascii de Datos Faltantes");			
		System.out.println("12. Exit");
		System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
	}

	public void printMessage(String mensaje) {

		System.out.println(mensaje);
	}		


	public void dar1A(int N, MaxHeapCP<ArregloDinamico<Zona>> heap)
	{
		for (int i=0; i<N; i++)
		{
			ArregloDinamico<Zona> arreglo = heap.sacarMax();
			System.out.println("Letra: " + arreglo.get(0).scaNombre.charAt(0) + " | Numero de zonas: " + arreglo.size());
			System.out.println("Mostrando primeras 20 zonas:");
			for (int j=0; j<arreglo.size() && j<20; j++)
			{
				Zona zona = arreglo.get(j);
				System.out.println("\tNombre: " + zona.scaNombre);
			}
			System.out.println("––––––");
		}
	}


	public void dar2A(LinearProbingHashST<String, NodoLatLong> tabla)
	{
		int c = 0;
		Iterator<String> llaves = tabla.keys();
		System.out.println("Total nodos encontrados: " + tabla.getN());
		while (llaves.hasNext() && c < 20)
		{
			String actual = llaves.next();
			String[] p = actual.split(",");
			String nombre = p[0];
			NodoLatLong coordenada = tabla.getVal(actual);
			System.out.println("Nombre zona: " + nombre + " | Longitud: " + coordenada.getLongitud() + " | Latitud: " + coordenada.getLatitud());
			c++;
		}
	}

	public void dar3A(Iterator<Viaje> iter)
	{
		int c = 0;
		ArrayList<Viaje> lista = new ArrayList<Viaje>();
		while(iter.hasNext())
		{
			lista.add(iter.next());
			c++;
		}
		System.out.println("Viajes encontrados: " + c);
		shellSort(lista);
		for (int i=0; i<lista.size() && i<20; i++)
			System.out.println(lista.get(i));
	}

	public void dar1B(MaxHeapCP<Zona> heap, int N)
	{
		for (int i=0; i<N && i<20; i++) 
		{
			Zona zona = heap.sacarMax();
			String nombre = zona.scaNombre;
			Double lat = zona.maxLatitud().getLatitud();
			Double lon = zona.maxLatitud().getLongitud();

			System.out.println("Nombre zona: " + nombre + " | Punto mas al norte: Longitud: " + lon + " Latitud: " + lat);

		}
	}

	public void dar2B(LinearProbingHashST<Integer, NodoLatLong> tabla)
	{
		int c = 0;
		Iterator<Integer> llaves = tabla.keys();
		System.out.println("Total nodos encontrados: " + tabla.getN());	
		while (llaves.hasNext() && c < 20)
		{
			NodoLatLong nodo = tabla.getVal(llaves.next());
			System.out.println("ID: " + nodo.getId() + " | Longitud: " + nodo.getLongitud() + " | Latitud: " + nodo.getLatitud() );
			c++;
		}
	}

	public void dar3B(Iterator<Viaje> iter)
	{
		int c = 0;
		ArrayList<Viaje> lista = new ArrayList<Viaje>();
		while(iter.hasNext())
		{
			lista.add(iter.next());
			c++;
		}
		System.out.println("Viajes encontrados: " + c);
		shellSort(lista);
		for (int i=0; i<lista.size() && i<20; i++)
		{
			Viaje viaje = lista.get(i);
			System.out.println("Zona de origen: " + viaje.getSourceid() + " | Zona de destino: " + viaje.getDstid() + " | Mes: " + viaje.getVariable() + " | Desviacion estandar: " + viaje.getStandardDeviationTravelTime());
		}
	}

	public void dar1C(LinearProbingHashST<String, Viaje> tabla)
	{
		int c = 0;
		Iterator<String> llaves = tabla.keys();
		System.out.println("Total viajes encontrados: " + tabla.getN());
		while (llaves.hasNext() && c < 20)
		{
			Viaje viaje = tabla.getVal(llaves.next());
			System.out.println("Zona de origen: " + viaje.getSourceid() + " | Zona de destino: " + viaje.getDstid() + " | Hora: " + viaje.getVariable() + " | Tiempo promedio: " + viaje.getMeanTravelTime());
			c++;
		}
	}

	public void dar3C(MaxHeapCP<ArregloDinamico<String>> heap, int N)
	{
		for (int i=0; i<N; i++)
		{
			ArregloDinamico<String> arreglo = heap.sacarMax();
			String nombre = arreglo.get(0);
			System.out.println("Nombre zona: " + nombre + " | Numero de puntos: " + (arreglo.size()-1));
		}
	}

	private void shellSort(ArrayList<Viaje> lista)
	{
		int N = lista.size();

		int h = 1;
		while (h < N/3)
			h = 3*h +1;

		while (h >= 1)
		{
			for (int i=h; i<N; i++)
			{
				for (int j=i; j>=h; j-=h)
				{
					if (lista.get(j).compareTo(lista.get(j-h)) < 0)
						exchange(lista, j, j-h);
					else
						break;
				}
			}
			h = h/3;
		}
	}

	private void exchange(ArrayList<Viaje> lista, int i, int j)
	{
		Viaje swap = lista.get(i);
		lista.set(i, lista.get(j));
		lista.set(j, swap);
	}
}
