package test.data_structures;

import model.data_structures.MaxHeapCP;
import model.logic.Viaje;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PruebaColaPrioridad {
	
	
	private MaxHeapCP<Viaje> maxHeap;
	
	
	@Before
	public void setUp1()
	{
		
		maxHeap = new MaxHeapCP<Viaje>();
	
	}
	
	
	public void setUp5()
	{
		for(int i =0; i<8; i++)
		{
			Viaje aAgregar = new Viaje(0, 0, 0, 0, i, 0);
			maxHeap.agregar(aAgregar);
		}
	}
	
	@Test
	public void agregarMaxHeapCP()
	{
		assertTrue(maxHeap.esVacia());
		assertTrue(maxHeap != null);
		assertEquals(0, maxHeap.darNumElementos());
		setUp5();
		assertEquals(8, maxHeap.darNumElementos());
	}
	
	@Test
	public void sacarMaxMaxHeapCP()
	{
		setUp5();
		assertEquals(8, maxHeap.darNumElementos());
		maxHeap.sacarMax();
		assertEquals(7, maxHeap.darNumElementos());
		maxHeap.sacarMax();
		assertEquals(6, maxHeap.darNumElementos());
		maxHeap.sacarMax();
		assertEquals(5, maxHeap.darNumElementos());
	}
	

}