package test.data_structures;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;

import model.data_structures.LinearProbingHashST;

public class TestLinearProbing {

	private LinearProbingHashST<Integer, Integer> tabla;


	@Before
	public void setUp()
	{
		tabla = new LinearProbingHashST<Integer, Integer>(7);
	}

	public void setUp2()
	{
		for (int i=0; i<10; i++)
			tabla.put(i,(2*i));
	}

	public void setUp3()
	{
		for (int i=0; i<10; i++)
		{
			tabla.put(i, i);
		}
	}

	public void setUp4()
	{
		setUp3();
		for (int i=10; i<20; i++)
			tabla.put(i, (2*i));
	}


	@Test
	public void testLinearProbingHashST()
	{
		assertNotNull(tabla.getKeys());
		assertEquals(7,tabla.getM());
		assertEquals(0,tabla.getN());
	}

	@Test
	public void testPutInSet()
	{
		setUp2();
		assertEquals(10, tabla.getN());
		assertEquals(17, tabla.getM());

		for (int i=0; i<tabla.getN(); i++)
		{
			assertEquals((int)(2*i), (int)tabla.getVal(i));
		}


	}

	@Test
	public void testGetVal()
	{
		for (int i=0; i<10; i++)
			assertNull(tabla.getVal(i));

		setUp2();

		for (int i=0; i<10; i++)
		{
			assertEquals((2*i), (int)tabla.getVal(i));
		}
	}

	@Test
	public void testDeleteVal()
	{
		for (int i=0; i<10; i++)
			assertNull(tabla.getVal(i));

		setUp2();
		for (int i=0; i<10;i++)
		{
			int val = tabla.deleteVal(i);
			assertEquals(2*i, val);
			assertFalse(tabla.contains(i));
		}
	}

	@Test
	public void testKeys()
	{
		setUp4();
		int i = 0;
		Iterator<Integer> iter = tabla.keys();
		while (iter.hasNext())
		{
			assertEquals((int)i++, (int)iter.next());
		}
	}

	@Test
	public void testResize()
	{
		setUp2();
		tabla.resize(20);
		assertEquals(20, tabla.getM());
	}

}
