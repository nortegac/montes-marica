package test.data_structures;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.*;

import model.data_structures.RedBlackBST;

public class TestRedBlackBT {
	
	private RedBlackBST<Integer, Integer> arbol;
	
	@Before
	public void setUp()
	{
		arbol = new RedBlackBST<Integer, Integer>();
	}
	
	
	public void setUp2()
	{
		for (int i=0; i<10; i++) 
		{
			arbol.put(i, i);
		}
	}
	
	@Test
	public void testRedBlackBST()
	{
		assertNotNull(arbol);
		assertEquals(0, arbol.size());
		
		assertTrue(arbol.check()); //Funciona como invariante
	}
	
	@Test
	public void testSize()
	{
		setUp2();
		assertEquals(10, arbol.size());
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testIsEmpty()
	{
		assertTrue(arbol.isEmpty());
		
		setUp2();
		assertFalse(arbol.isEmpty());
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testGet()
	{
		for (int i=0; i<10; i++)
			assertNull(arbol.get(i));
		
		setUp2();
		for (int i=0; i<10; i++)
			assertEquals((int)i, (int)arbol.get(i));
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testGetHeight()
	{
		for (int i=0; i<10; i++)
			assertEquals(-1, arbol.getHeight(i));
		
		setUp2();
		assertEquals(2, arbol.getHeight(0));
		assertEquals(1, arbol.getHeight(1));
		assertEquals(2, arbol.getHeight(2));
		assertEquals(0, arbol.getHeight(3));
		assertEquals(3, arbol.getHeight(4));
		assertEquals(2, arbol.getHeight(5));
		assertEquals(3, arbol.getHeight(6));
		assertEquals(1, arbol.getHeight(7));
		assertEquals(3, arbol.getHeight(8));
		assertEquals(2, arbol.getHeight(9));
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testContains()
	{
		for (int i=0; i<10; i++)
			assertFalse(arbol.contains(i));
		
		setUp2();
		for (int i=0; i<10; i++)
			assertTrue(arbol.contains(i));
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testPut()
	{
		for (int i=0; i<10; i++)
		{
			arbol.put(i, i);
			assertTrue(arbol.contains(i));
		}
		
		assertTrue(arbol.check()); 
	}
	
	@Test
	public void testHeight()
	{
		assertEquals(0, arbol.height());
		
		setUp2();
		assertEquals(3, arbol.height());
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testMin()
	{
		assertNull(arbol.min());
		
		setUp2();
		assertEquals(0, (int)arbol.min());
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testMax()
	{
		assertNull(arbol.max());
		
		setUp2();
		assertEquals(9, (int)arbol.max());
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testKeys()
	{
		Iterator<Integer> keys = arbol.keys();
		while(keys.hasNext())
		{
			fail(); //Iterador vacio
		}
		
		setUp2();
		keys = arbol.keys();
		int i = 0;
		while (keys.hasNext())
		{
			assertEquals(i++, (int)keys.next());
		}
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testValuesInRange()
	{
		Iterator<Integer> vals = arbol.valuesInRange(arbol.min(), arbol.max());
		while(vals.hasNext())
		{
			fail(); //Iterador vacio
		}
		
		setUp2();
		int i = 0;
		vals = arbol.valuesInRange(0, 4);
		while (vals.hasNext())
		{
			assertEquals(i++, (int)vals.next());
		}

		vals = arbol.valuesInRange(5, 9);
		while (vals.hasNext())
		{
			assertEquals(i++, (int)vals.next());
		}
		
		assertTrue(arbol.check());
	}
	
	@Test
	public void testKeysInRange()
	{
		Iterator<Integer> keys = arbol.keysInRange(arbol.min(), arbol.max());
		while(keys.hasNext())
		{
			fail(); //Iterador vacio
		}
		
		setUp2();
		int i = 0;
		keys = arbol.keysInRange(0, 4);
		while (keys.hasNext())
		{
			assertEquals(i++, (int)keys.next());
		}
		
		keys = arbol.keysInRange(5, 9);
		while (keys.hasNext())
		{
			assertEquals(i++, (int)keys.next());
		}
		
		assertTrue(arbol.check());
	}
	
	
}

